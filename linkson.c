#include "linkson.h"
#include <stdlib.h>
#include <stdio.h>

JNODE *create_jnode() {
    JNODE *node = malloc(sizeof(JNODE));
    node->start = 0;
    node->end = 0;
    node->parent = NULL;
    node->next = NULL;
    return node;
}

void free_list(JNODE *head) {
    if(head->next != NULL) {
        free_list(head->next);
    }
    free(head);
}

JNODE *parse(const char *js) {
    JNODE *head = create_jnode();

    int i=0, count=0;

    while(js[i] != '{' || js[i] != '[') {
        i++;
    }

    head->start = i++;

    _Bool inString = 0;
    JNODE *next = create_jnode();
    next->parent = head;

    while(js[i] != '\0') {
        switch(js[i]) {
            case '{':
            case '[':
                if(!inString) {
                    count++;
                }
            case '}':
            case ']':
                if(!inString) {
                    count--;
                }
            case '"':
                if(!inString) {
                    count++;
                } else {
                    count--;
                }
                inString = !inString;
            default:
                break;
        }
        i++;
    }

    return head;
}

int main() {
    int i=0;
    printf("%i\n",++i);
    printf("%i\n",i++);
    return 0;
}
