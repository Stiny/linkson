#ifndef _LINKSON_
#define _LINKSON_

typedef struct JNODE {
    int start;
    int end;
    struct JNODE *parent;
    struct JNODE *next;
} JNODE;

JNODE *create_jnode();

void free_list(JNODE *head);

JNODE *parse(const char *js);

#endif
