all: linkson.o
	gcc -o linkson linkson.o

%.o: %.c %.h
	gcc -c $< -o $@

clean:
	rm *.o
	rm linkson
