linked-json
===========

#Introduction
This project is an attempt at creating a JSON parser that validates in one pass of a string by using a linked list as a data structure.

##Algorithm

    :::c
      list_head parse(const char* str) {
        int i=0;
        while(str[i] != '\0') {
          interpret_characters_to_generate_nodes();
        }
        return list_head;
      }

##Logic
By using a linked list we can build the list organically rather than allocate space for an array beforehand.
